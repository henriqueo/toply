# toply

A text-based lyrics application.

Pronunciation: Tango-Oscar-Papa-Lima-Yankee

# Features

- [x] Listening to DBUS compatible players
- [x] Scraping lyrics from preset sources
- [ ] Lyrics file synchronization (.lrc)
- [x] Synchonized lyrics on the CLI

# TO-DO

See the [issue board](https://gitlab.com/henriqueo/toply/-/boards/1195251).

## Licensing

Copyright (C) 2021 Henrique Oliveira

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
