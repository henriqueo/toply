class MoreThanOnePlayerRunningException(Exception):
    pass

class LyricsNotFoundException(Exception):
    pass
