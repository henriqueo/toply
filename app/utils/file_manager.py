from pathlib import Path
import os
import glob

def get_toply_lrc_dir_path():
    lrc_dir_path = os.path.join(Path.home(), '.toply/lyrics')

    if not os.path.isdir(lrc_dir_path):
        os.makedirs(lrc_dir_path)

    return lrc_dir_path

def get_lrc_files():
    lrc_dir_path = get_toply_lrc_dir_path()

    lrc_files = glob.glob('{}/*.lrc'.format(lrc_dir_path))

    return lrc_files

def get_local_lrc_path(artist, title):
    lrc_files = get_lrc_files()

    lrc_file_wanted = '{} - {}.lrc'.format(artist, title)

    lrc_files_match = list(
        filter(lambda f: lrc_file_wanted in f,lrc_files))

    if len(lrc_files_match) > 0:
        return lrc_files_match[0]

