import dbus
import dbus.mainloop.glib
import pgi
pgi.install_as_gi()
from gi.repository import GLib

from models import player
import lyrics_manager as lm


class PlayerWatcher:
    def __init__(self):
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

        self.lyrics_manager = lm.LyricsManager()

        # Initializing session bus
        self.bus_session = dbus.SessionBus()

        self.player = player.Player(self.bus_session)

        self.status = player.Status(self.player.get_metadata())

        # Listening to signals from MediaPlayer2.Player interfaces:
        # TODO Be more specific about which signals are needed.
        self.bus_session.add_signal_receiver(
            self.manage_player_signal,
            dbus_interface='org.mpris.MediaPlayer2.Player')

        loop = GLib.MainLoop()

        loop.run()

    def manage_player_signal(self, *args, **kwargs):
        previous_artist = self.status.metadata.get('xesam:artist')

        previous_title = self.status.metadata.get('xesam:title')

        current_status = player.Status(
            self.player.get_metadata(),
            self.player.get_position())

        current_artist = current_status.metadata.get('xesam:artist')

        current_title = current_status.metadata.get('xesam:title')

        if current_artist != previous_artist or \
            current_title != previous_title:
            print('Changed song')

            self.status = current_status

            self.lyrics_manager.display_lyrics(
                ' '.join(current_artist), current_title)

        elif self.status.position != current_status.position:
            print('Just seeked')
