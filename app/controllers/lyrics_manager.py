import os
from pathlib import Path

import pylrc

from views import lyrics_view
from models import lyrics_scraper as ls
from utils import file_manager


class LyricsManager():
    def __init__(self):
        pass

    def get_lyrics(self, artist, title):
        lyrics_scraper = ls.LyricsScraper()

        lyrics_lines = list()

        local_lrc_path = file_manager.get_local_lrc_path(artist, title)

        if local_lrc_path is None:
            lyrics_lines = lyrics_scraper.search(artist, title)

            lyrics_list = map(
                lambda l: pylrc.classes.LyricLine('[0:0]',l), lyrics_lines)

            new_lrc = pylrc.classes.Lyrics(lyrics_list)

            new_lrc.artist = artist

            new_lrc.title = title

            new_lrc_path = os.path.join(
                file_manager.get_toply_lrc_dir_path(),
                '{} - {}.lrc'.format(artist,title))

            with open(new_lrc_path, 'w') as new_lrc_file:
                new_lrc_file.write(new_lrc.toLRC())

                return new_lrc

        else:
            with open(local_lrc_path, 'r') as local_lrc:
                lrc_str = ''.join(local_lrc.readlines())

                return pylrc.parse(lrc_str)

    def display_lyrics(self, artist, title):
        lyrics = self.get_lyrics(artist, title)

        lv = lyrics_view.LyricsView(lyrics)

