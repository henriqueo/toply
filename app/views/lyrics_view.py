import curses
import time
import os
import threading

class LyricsView:
    def __init__(self, lyrics_list):
        self.lyrics_list = lyrics_list

        curses.wrapper(self.curses_main)

    def print_to_cli(self, stdscr):
        curses.curs_set(False)

        height, width = stdscr.getmaxyx()

        # os.system('color 1f')

        start_y = int((height // 2) - 2)

        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_GREEN)

        for line_iterator, lyrics_item in enumerate(self.lyrics_list):
            len_lyrics_line = len(lyrics_item.text)

            start_x_lyrics_line = int(
                (width // 2) - (len_lyrics_line // 2) - len_lyrics_line % 2)

            if line_iterator == 0:
                wait_time = lyrics_item.time * 1000

            elif line_iterator < len(self.lyrics_list) - 1:
                wait_time = (
                    lyrics_item.time - self.lyrics_list[line_iterator-1].time
                ) * 1000

            curses.napms(int(wait_time))

            stdscr.clear()

            stdscr.addstr(
                start_y,
                start_x_lyrics_line,
                '{}'.format(lyrics_item.text),
                curses.color_pair(1))

            stdscr.refresh()

    def curses_main(self, stdscr):
        stdscr.clear()

        stdscr.refresh()

        curses.cbreak()

        key = 0

        while key != ord('q'):
            stdscr.clear()

            stdscr.refresh()

            self.print_to_cli(stdscr)

            # Wait for next input
            k = stdscr.getch()

        curses.nocbreak()

        stdscr.keypad(False)

        curses.echo()

        curses.endwin()

