import dbus

import tp_exceptions


class Status:
    def __init__(self, metadata, position=0):
        self.metadata = metadata
        self.position = position

class Player:
    def __init__(self, session):
        self.bus_session = session

        self.player = self.get_player()

    def get_player(self):
        players = list(
            filter(lambda x: 'MediaPlayer' in x, self.bus_session.list_names())
        )

        if len(players) > 1:
            raise tp_exceptions.MoreThanOnePlayerRunningException

        return self.bus_session.get_object(
            players[0], '/org/mpris/MediaPlayer2')

    def get_player_interface(self, player):
        return dbus.Interface(
            self.player, dbus_interface='org.mpris.MediaPlayer2.Player')

    def __get_property(self, prop):
        return self.player.Get(
            'org.mpris.MediaPlayer2.Player',
            prop,
            dbus_interface='org.freedesktop.DBus.Properties')

    def get_position(self):
        return self.__get_property('Position')

    def get_metadata(self):
        return self.__get_property('Metadata')
