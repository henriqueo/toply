import re
import urllib

import requests
from lxml import etree
import lxml
from lxml.html.clean import clean_html

from utils import config, tp_exceptions


session = requests.Session()

session.headers.update(config.get_config().get('headers'))

class LyricsScraper:
    def __init__(self):
        pass

    def __validate_source(self, source):
        url_regex = re.compile(r'https?:\/\/\w+\.\w+')

        validate_url = lambda x: url_regex.match(x) != None

        validate_method = lambda x: x in ['GET', 'POST']

        validate_query = lambda x: type(x) == str

        validate_xpath_result = lambda x: x == x

        validate_xpath_lyrics = lambda x: x == x

        valid_props = [
            validate_url(source.get('url')),
            validate_method(source.get('method')),
            validate_query(source.get('query')),
            validate_xpath_result(source.get('xpath_result')),
            validate_xpath_lyrics(source.get('xpath_lyrics'))
        ]

        return False not in valid_props

    def search(self, artist, title):
        sources = config.get_config().get('sources')

        lyrics_list = list()

        for key, source in sources.items():
            # TODO Handle invalid sources with a new exception.
            if not self.__validate_source(source): continue

            query = { source.get('query'): '{} {}'.format(artist, title) }

            url = source.get('url')

            if len(source.get('query')) == 0:
                url = urllib.parse.urljoin(
                    url,
                    query[''])

            req = requests.Request(
                url=url,
                method=source.get('method')
            )

            prepped = req.prepare()

            prepped.headers = config.get_config().get('headers')

            if source.get('method') == 'GET':
                prepped.params = query

            elif source.get('method') == 'POST':
                prepped.data = query

            try:
                res = session.send(prepped)

                tree = lxml.html.fromstring(res.content)

                parsed_url = urllib.parse.urlparse(source.get('url'))

                # All links must be absolute
                tree = lxml.html.make_links_absolute(
                    tree,
                    '{uri.scheme}://{uri.netloc}'.format(uri=parsed_url))

                # Top result in search results.
                best_result = tree.xpath(source.get('xpath_result'))

                if len(best_result) == 0:
                    raise tp_exceptions.LyricsNotFoundException()

                # URL of the top result.
                top_result_content = session.get(
                    best_result[0].get('href')).content

                clean_top_result_content = clean_html(top_result_content)

                lyrics_html = etree.HTML(clean_top_result_content)

                lyrics_content = lyrics_html.xpath(source.get('xpath_lyrics'))

                # Hot fix for pages that return an array of strings
                lyrics_content = ''.join(lyrics_content).splitlines()

                # Cleaning leading and tracing chars
                lyrics_map = map(lambda x: x.strip(), lyrics_content)

                # Removes empty lines
                lyrics_list = list(filter(lambda x: len(x) > 0, lyrics_map))

                # If less then 10 lines are found, skip to next source.
                if len(lyrics_list) < 10: continue

                return lyrics_list

            except requests.RequestException:
                pass
            except lxml.XMLSyntaxError:
                pass
            except tp_exceptions.LyricsNotFoundException:
                pass

        raise LyricsNotFoundException

